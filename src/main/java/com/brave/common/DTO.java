package com.brave.common;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * Data Transfer object, including Command, Query and Response,
 *
 * Command and Query is CQRS concept.
 *
 * @author zhangjun 2
 */
@Data
public abstract class DTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public LocalDateTime createTime;
	public LocalDateTime updateTime;

}
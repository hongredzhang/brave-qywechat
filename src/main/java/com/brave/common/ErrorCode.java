package com.brave.common;

/**
 * @author junzhang
 */
public class ErrorCode {
	public static final String SERVER_ERROR = "400";
	public static final String NO_AUTH_TOKEN = "401";
	public static final String CUSTOMER_EXISTS = "402";
}

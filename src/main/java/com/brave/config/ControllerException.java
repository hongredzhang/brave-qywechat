package com.brave.config;


import com.brave.common.ErrorCode;
import com.brave.common.Response;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author junzhang
 */
@Slf4j
@ControllerAdvice
public class ControllerException {
	@ExceptionHandler(Exception.class)
	@ResponseBody
	Response handleControllerException(Exception ex) {
		ex.printStackTrace();
		return Response.buildFailure(ErrorCode.SERVER_ERROR,ex.getMessage());
	}

}
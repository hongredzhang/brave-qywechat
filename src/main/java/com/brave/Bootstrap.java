package com.brave;

import feign.Request;
import feign.Retryer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 *
 */
@SpringBootApplication
@EnableFeignClients
public class Bootstrap
{
    public static void main( String[] args ){
		SpringApplication.run(Bootstrap.class,args);
    }
	@Bean
	Request.Options options() {
		return new Request.Options(1000*5,5*1000);
	}

	@Bean
	Retryer retryer() {
		return Retryer.NEVER_RETRY;
	}
}

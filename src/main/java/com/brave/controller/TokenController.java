package com.brave.controller;

import com.brave.common.SingleResponse;
import com.brave.service.TokenService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/token/v1")
public class TokenController {
	@Autowired
	TokenService tokenService;

	@GetMapping("/contact")
	SingleResponse<String> contactToken() {
		String result = tokenService.contactToken();
		log.info("获取到的通讯录token是{}",result);
		return SingleResponse.buildSuccess();
	}

}

package com.brave.client.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenDTO extends WXBaseDTO implements Serializable {
	String access_token;
	Integer expires_in;
}

package com.brave.infrastructure.dao;

import com.brave.client.dto.Department;
import com.brave.infrastructure.dao.util.MongoCollectionName;

import org.springframework.stereotype.Repository;

@Repository
public class DepartmentDAO extends BaseDAO{

	public void addDepartment(Department department) {
		mongoTemplate.save(department, MongoCollectionName.DEPARTMENT);
	}
}

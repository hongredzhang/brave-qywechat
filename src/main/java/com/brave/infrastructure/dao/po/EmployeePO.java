package com.brave.infrastructure.dao.po;

import lombok.Data;

@Data
public class EmployeePO {
	String userid;
	String name;
	Integer[] department;
	String open_userid;
}

package com.brave.infrastructure.dao.po;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TokenPO {
	String type;
	String accessToken;
	Integer expiresIn;
	Integer status;
	String createTime;
}
